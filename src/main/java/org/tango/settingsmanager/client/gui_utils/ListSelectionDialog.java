//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/RipsProto/src/rips_proto/commons/ListSelectionDialog.java,v $
//
// Project:   Tango
//
// Description:  Basic Dialog Class to display info
//
// $Author: verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2009,2010,2011,2012,2013,2014,2015,2016
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Revision: 1.1.1.1 $
//
// $Log:  $
//
//-======================================================================

package org.tango.settingsmanager.client.gui_utils;

import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;


//===============================================================
/**
 *	JDialog Class to display info
 *
 *	@author  Pascal Verdier
 */
//===============================================================


@SuppressWarnings({"MagicConstant", "Convert2Diamond"})
public class ListSelectionDialog extends JDialog {

	private int returnValue = JOptionPane.OK_OPTION;
	static final int NEW_PROJECT = -1;
	//===============================================================
	/**
	 *	Creates new form ListSelectionDialog
	 */
	//===============================================================
	public ListSelectionDialog(JFrame parent, List<String> items, String title) {
        this(parent, items, title, true);
    }
	//===============================================================
	/**
	 *	Creates new form ListSelectionDialog
	 */
	//===============================================================
	public ListSelectionDialog(JFrame parent, List<String> items, String title, boolean creation) {
		super(parent, true);
  		initComponents();
        newBtn.setVisible(creation);
        jList.setListData(items.toArray(new String[items.size()]));
        jList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
               listActionPerformed(event);
            }
        });

        int maxLength = 0;
        for (String item : items)
            if (item.length()>maxLength)
                maxLength = item.length();

        jList.setFont(new Font("Monospaced", Font.BOLD, 12));
        int width  = maxLength*8;       if (width>800)  width  = 800;
        int height = 20*items.size();   if (height>800) height = 800;

        jScrollPane.setPreferredSize(new Dimension(width, height));
        titleLabel.setText(title);
		pack();
 		ATKGraphicsUtils.centerDialog(this);
	}

	//===============================================================
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
	//===============================================================
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.JPanel topPanel = new javax.swing.JPanel();
        titleLabel = new javax.swing.JLabel();
        javax.swing.JPanel bottomPanel = new javax.swing.JPanel();
        newBtn = new javax.swing.JButton();
        javax.swing.JLabel jLabel1 = new javax.swing.JLabel();
        javax.swing.JButton okBtn = new javax.swing.JButton();
        javax.swing.JButton cancelBtn = new javax.swing.JButton();
        jScrollPane = new javax.swing.JScrollPane();
        jList = new javax.swing.JList<String>();
        javax.swing.JLabel rightLabel = new javax.swing.JLabel();
        javax.swing.JLabel leftLabel = new javax.swing.JLabel();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        titleLabel.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        titleLabel.setText("Dialog Title");
        topPanel.add(titleLabel);

        getContentPane().add(topPanel, java.awt.BorderLayout.NORTH);

        newBtn.setText("New");
        newBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newBtnActionPerformed(evt);
            }
        });
        bottomPanel.add(newBtn);

        jLabel1.setText("        ");
        bottomPanel.add(jLabel1);

        okBtn.setText("OK");
        okBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okBtnActionPerformed(evt);
            }
        });
        bottomPanel.add(okBtn);

        cancelBtn.setText("Cancel");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });
        bottomPanel.add(cancelBtn);

        getContentPane().add(bottomPanel, java.awt.BorderLayout.SOUTH);

        jScrollPane.setViewportView(jList);

        getContentPane().add(jScrollPane, java.awt.BorderLayout.CENTER);

        rightLabel.setText("        ");
        getContentPane().add(rightLabel, java.awt.BorderLayout.EAST);

        leftLabel.setText("        ");
        getContentPane().add(leftLabel, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	//===============================================================
	//===============================================================
    private void listActionPerformed(MouseEvent event) {
        if (event.getClickCount()==2) {
            okBtnActionPerformed(null);
        }
    }
	//===============================================================
	//===============================================================
    @SuppressWarnings("UnusedParameters")
	private void okBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okBtnActionPerformed
		returnValue = JOptionPane.OK_OPTION;
		doClose();
	}//GEN-LAST:event_okBtnActionPerformed
	//===============================================================
	//===============================================================
	@SuppressWarnings("UnusedParameters")
    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
		returnValue = JOptionPane.CANCEL_OPTION;
		doClose();
	}//GEN-LAST:event_cancelBtnActionPerformed
	//===============================================================
	//===============================================================
    @SuppressWarnings("UnusedParameters")
	private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
		returnValue = JOptionPane.CANCEL_OPTION;
		doClose();
	}//GEN-LAST:event_closeDialog
    //===============================================================
    //===============================================================
    @SuppressWarnings("UnusedParameters")
    private void newBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newBtnActionPerformed
        returnValue = NEW_PROJECT;
        doClose();
    }//GEN-LAST:event_newBtnActionPerformed

	//===============================================================
	/**
	 *	Closes the dialog
	 */
	//===============================================================
	private void doClose() {
        setVisible(false);
        dispose();
	}
	//===============================================================
	//===============================================================
    public String getSelectedItem() {
        if (jList.getSelectedValue()==null)
            return null;
        return jList.getSelectedValue();
    }
	//===============================================================
	//===============================================================
	public int showDialog() {
		setVisible(true);
		return returnValue;
	}

	//===============================================================
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList<String> jList;
    private javax.swing.JScrollPane jScrollPane;
    private javax.swing.JButton newBtn;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
	//===============================================================
}
