# SettingsManager
A tool based on files to standardize and centralize the settings management for the control system.

It needs JTango.jar to compile or to run.

The release jar files are stored at
   https://bintray.com/tango-controls/maven/SettingsManager#files/org/tango/ds/SettingsManager


[ ![Download](https://api.bintray.com/packages/tango-controls/maven/SettingsManager/images/download.svg) ](https://bintray.com/tango-controls/maven/SettingsManager/_latestVersion)
[![Docs](https://img.shields.io/badge/Latest-Docs-orange.svg)](http://www.esrf.eu/computing/cs/tango/misc_doc/settings_man_doc/index.html)
